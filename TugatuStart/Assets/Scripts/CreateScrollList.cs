﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]   // Serializable class
public class buttonItem { // ONLY name and icon are used on prefab sampleButton in this version
	public string name;
	public Sprite icon;
	//public string type;
	//public string count;
	//public bool isUser;
	//public Sprite statusIcon;
	public Button.ButtonClickedEvent thingToDo;
}

public class CreateScrollList : MonoBehaviour {
	
	public GameObject sampleButton;  // Ref to PreFab SampleButton, in Editor, Drag and drop instance here
	public List<buttonItem> itemList;
	
	public Transform contentPanel;   // this is where list will be displayed
	
	void Start () {
		Debug.Log ("Populating list");
		PopulateList ();

	}
	
	void PopulateList () {
		foreach (var item in itemList) {
			GameObject newButton = Instantiate (sampleButton) as GameObject;
			// simple form of instantiate without position info
			SampleButton button = newButton.GetComponent <SampleButton> (); // find SampleButton script
			button.nameLabel.text = item.name;
			button.icon.sprite = item.icon;
			Debug.Log ("item.name:" + item.name);
			//button.typeLabel.text = item.type;
			//button.countLabel.text = item.count;
			//button.statusIcon.SetActive (item.isUser);
			//if (item.statusIcon != null)
			//button.statusIcon.sprite = item.statusIcon;

			// NOTE:if script is set to gameobject then the following invokes method 
			//button.button.onClick = item.thingToDo;  // set name of method to invoke for onClick event
			// Note: If a standalone mtheod is associated with prefab, then it can be invoked as long as 
			// onclick above it also not being used.

			newButton.transform.SetParent (contentPanel);
		}
	}
	
	public void SomethingToDo () {
		Debug.Log ("button: I done did something!");
	}
	
	//public void SomethingElseToDo (GameObject item) {
	//	Debug.Log (item.name);
	//}
}


