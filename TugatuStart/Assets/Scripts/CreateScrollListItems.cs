﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]   // Serializable class
public class Item {
	public string name;
	public Sprite icon;
	public string type;
	public string id;
	public bool isUser;
	public Sprite statusIcon;
	public Button.ButtonClickedEvent thingToDo;
}

public class CreateScrollListItems : MonoBehaviour {
	
	public GameObject itemobj;  // Ref to PreFab SampleButton, in Editor, Drag and drop instance here
	public List<Item> itemList;
	
	public Transform contentPanel;   // this is where list will be displayed
	
	void Start () {
		PopulateListItems ();
	}
	
	void PopulateListItems () {
		foreach (var item in itemList) {
			GameObject newButton = Instantiate (itemobj) as GameObject;
			// simple form of instantiate without position info
			DetailedButton button = newButton.GetComponent <DetailedButton> ();
			button.nameLabel.text = item.name;
			button.icon.sprite = item.icon;
			button.typeLabel.text = item.type;
			button.idLabel.text = item.id;
			//button.statusIcon.SetActive (item.isUser);
			if (item.statusIcon != null)
			button.statusIcon.sprite = item.statusIcon;
			// if script is set to gameobject then the following invokes method 
			//button.button.onClick = item.thingToDo;  // set name of method to invoke for onClick event
			// Note: If a standalone mtheod is associated with prefab, then it can be invoked as long as 
			// onclick above it also not being used.

			newButton.transform.SetParent (contentPanel);
		}
	}
	
	public void SomethingToDo () {
		Debug.Log ("I done did something!");
	}
	
	public void SomethingElseToDo (GameObject item) {
		Debug.Log (item.name);
	}
}


