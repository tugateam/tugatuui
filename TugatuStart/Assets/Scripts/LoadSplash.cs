﻿using UnityEngine;
using System.Collections;

/*
 * NOTE:  To use this, invoke the following function in annother event / object,  
 * from a script within which there would be a need to switch to a different 
 * scene during run time:
 * 
 * public static void loadLevel(string sceneName)
{
        LoadSplash.show();
        Application.LoadLevel(sceneName);
}

 * NOTE:  NOT USED CURRENTLY
 * */

public class LoadSplash : MonoBehaviour
{
	//We make a static variable to our LoadingScreen instance
	static LoadSplash instance;
	//reference to gameobject with the static image 
	GameObject loadingScreenImage;
	
	//function which executes on scene awake before the start function
	void Awake()
	{
		Debug.Log ("Awake called");
		//find the ImageLS gameobject from the Hierarchy
		loadingScreenImage = GameObject.Find("ImageLS");
		//destroy the already existing instance, if any
		if (instance)
		{
			Debug.Log ("instance exists, destroy in awake ");
			Destroy(gameObject);
			hide();     //call hide function to hide the 'loading Screen Sprite'
			return;
		}
		instance = this;    
		instance.loadingScreenImage.SetActive(false);
		DontDestroyOnLoad(this);  //make this object persistent between scenes
	}
	
	void Update()
	{
		//hide the loading screen if the scene is loaded
		// a new level is loaded after the current game frame finishes. 
		//isLoadingLevel returns true if a level load was requested this 
		//frame already.

		if(!Application.isLoadingLevel)
			hide();
	}
	//function to enable the loading screen
	public static void show()
	{
		Debug.Log ("show() called");
		//if instance does not exists return from this function
		if (!InstanceExists()) 
		{
			Debug.Log ("instance does not exists, show() ");
			return;
		}

		Debug.Log ("instance exists, show() ");
		//enable the loading image object 
		instance.loadingScreenImage.SetActive(true);
	}
	//function to hide the loading screen
	public static void hide()
	{
		if (!InstanceExists()) 
		{
			return;
		}
		instance.loadingScreenImage.SetActive(false);
	}
	//function to check if the persistent instance exists
	static bool InstanceExists()
	{
		if (!instance)
		{
			return false;
		}
		return true;
		
	}
	
}
