﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DetailedButton : MonoBehaviour {
	
	public Button button;
	public Text nameLabel;
	public Image icon;
	public Text typeLabel;
	public Text idLabel;
	public Image statusIcon;
}
