﻿using UnityEngine;
using System.Collections;

public class SplashStartScene : MonoBehaviour {


	//reference to gameobject with the static image 
	GameObject loadingScreenImage;
	float timer = 0.0f;
	float timerMax = 3.0f;
	bool showsplash = true;

	void Awake() 
	{ 
		timer = timerMax ; 
		loadingScreenImage = GameObject.Find("ImageLS");
	} 

	void Update() 
	{ 
		if (showsplash) {

			if (timer == timerMax) {
				// show panel
				loadingScreenImage.SetActive (true);

			}
			timer -= Time.deltaTime; 
			if (timer < 0) { 
				Debug.Log ("timer Zero reached !"); // reset timer timer = timerMax; 
				loadingScreenImage.SetActive (false); // hide panel
				showsplash = false;
			} 
		}
	}

	public void StartAtTugatu() 
	{ 
		// ... load the level for the game / tugatu island display
		Application.LoadLevel(2);
	} 

}
